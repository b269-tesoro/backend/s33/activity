
// #3 fetch request - GET method
fetch("https://jsonplaceholder.typicode.com/todos")
.then(response => response.json())

// #4 map method to return title
.then((json) => json.map(function (json) {
	let toDos = [];
	toDos += json.title;
	return toDos;
}))
// #4 print result to console
.then((toDos) => console.log(toDos));

// #5 fetch requeest = GET single to do list item
fetch("https://jsonplaceholder.typicode.com/todos/1")
.then(response => response.json())
.then((json) =>
	// #6 print title and status of the item
	console.log(`The item "${json.title}" on the list has a status of ${json.completed}`));

// #7 fetch request - POST
fetch("https://jsonplaceholder.typicode.com/todos", {
	method: "POST",
	headers:
	{
		"Content-Type" : "application/json"
	},
	body: JSON.stringify({
		completed: false,
		title: "Created To Do List Item",
		userId: 1,
	}) 
})
.then(response => response.json())
.then((json) => console.log(json));

// #8 fetch request - PUT
fetch("https://jsonplaceholder.typicode.com/todos/1", {
	method: "PUT",
	headers:
	{
		"Content-Type" : "application/json"
	},
	// #9 changing data structure
	body: JSON.stringify({
		dateCompleted: "Pending",
		description: "To update the my to do list with a different data structure",
		status: "Pending" ,
		title: "Updated To Do List Item",
		UserId: 1
	}) 
})
.then(response => response.json())
.then((json) => console.log(json));

// #10 fetch request - PATCH
fetch("https://jsonplaceholder.typicode.com/todos/1", {
	method: "PATCH",
	headers:
	{
		"Content-Type" : "application/json"
	},
	// #11 update status, date, and title
	body: JSON.stringify({
			dateCompleted: "07/09/21",
			status: "Complete" ,
			title: "delectus aut autem",
			UserId: 1
		}) 
})
.then(response => response.json())
.then((json) => console.log(json));

// #12 fetch request - DELETE
fetch("https://jsonplaceholder.typicode.com/todos/1", {method: "DELETE"})

